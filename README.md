# Push to Repository via GitLab CI

This project serves as a guide on how to use GitLab CI for pushing back
information from inside a CI job.

## GitLab UI Setup

1. Create an SSH key pair in your terminal with below commands.

```bash
$ # Create an SSH key pair
$ ssh-keygen -t ed25519 -C gitlab-ci -f gitlab-ci-key -P ""
$ # Copy the public key
$ cat gitlab-ci-key.pub
ssh-ed25519 <public-key> gitlab-ci
```

2. On the left menu bar of your GitLab project go to `Settings -> Repository`
   and expand the _Deploy Keys_ section.
   Create a new deploy key by copying the content of your SSH public key you
   outputted via the `cat gitlab-ci-key.pub`.
   Don't forget to tick the _Write access allowed_ checkbox.

   ![Configure Deploy Keys](images/configure-deploy-key.png "Configure Deploy Keys")

3. On the left menu bar of your GitLab project go to `Settings -> CI/CD`
   and expand the _Variables_ section.
   Click on _Add variable_.  
   Name the variable `SSH_PRIVATE_KEY`, copy paste the content of the
   `gitlab-ci-key` file in the _Value_ field.  
   Make sure to tick the checkbox `Protect variable`.

   ![Add Secret Variable](images/create-secret-variable.png "Add Secret Variable")

4. Configure a second variable called `SSH_SERVER_HOST_KEYS`
   and copy paste the host keys from below.
   ```
   gitlab.hzdr.de ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDQCnCApbrY9ROEXWh/3myugH8JlsSEiousqQAKX2u/acMLE5U7DM/xh6LKJiPzEiFGmTOqF3cSFmpF/qnrnMINCaRQJMbY/2/pUPf21tLStyWcEVeTBgzvjYjNblZwgOpaC2BuwMoLCP4sPael6yKWu582lHhzs+WAlagfAe+k8NXUp4cGKZLCG6SI2L2i3Xw4I4O/I+4zL1hYLOXilV/fGu9YBGw7W8EpaIqOAmB+nWJKo0/g1cPXrDftOWkavRyJe3WHlCbNTsUVEy2A51YpAfNJ/Oc+pF47r8xLqe38hzpIOR5NUM2VFiJJ8HjSAmjPyY8+mVN8NfV2Wuik5s1v
   gitlab.hzdr.de ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBFUSk3xGzP48tkekGpui8ikVjLeKKhZC0UgGqWoogTsU5nVmEXmSpw6r7L+ddXLDnZfNTOfy0W8mXLfoiVcJ3sA=
   gitlab.hzdr.de ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIGIG+t1jlOXZgHGWUUI7D3W5DYObvrVqiUMbKX0+m0pf
   ```
   ![Add server host keys variable](images/add-server-host-keys.png "Add server host keys variable")

With this everything is setup in the GitLab UI.

## GitLab CI configuration

Now, Gitlab CI can be activated by adding a file called `.gitlab-ci.yml` in the
root of your repository.
This project contains a suitable basis for the [`.gitlab-ci.yml`](.gitlab-ci.yml)
file you can build upon.

The example changes a file called `ci-execution-times.txt` on each run of the CI
pipeline and pushes the changes back to the repository (`script` section).
In the `after_script` section, first the SSH client is installed and configured,
and second, the change is pushed back via Git.
